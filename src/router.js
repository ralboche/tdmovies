import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      alias: "/movies",
      name: "movies",
      props: true,
      component: () => import("./views/MoviesList"),
    },
    {
      path: "/movie/:id",
      name: "movie-details",
      props: true,
      component: () => import("./views/Movie"),
    },
    {
      path: "/movie/:id/edit",
      name: "movie-edit",
      props: true,
      component: () => import("./views/EditMovie"),
    },
    {
      path: "/movies/add",
      name: "movies-add",
      props: true,
      component: () => import("./views/AddMovie"),
    },
    {
      path: "/producers/add",
      name: "producers-add",
      props: true,
      component: () => import("./views/AddProducer"),
    },
  ],
});
