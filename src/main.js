import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import vuetify from "./plugins/vuetify";

import Header from "./components/Header.vue";
import Footer from "./components/Footer.vue";

Vue.config.productionTip = false;

Vue.component("Header", Header);
Vue.component("Footer", Footer);

new Vue({
  router,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
