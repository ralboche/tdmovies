import http from "../../http-common";

class ProducerDataService {
  getAll() {
    return http.get("/producers");
  }

  get(id) {
    return http.get(`/producers/${id}`);
  }

  create(data) {
    return http.post("/producers", data);
  }
}

export default new ProducerDataService();
