import http from "../../http-common";

class MovieDataService {
  getAll(search) {
    return http.get(`/movies?search=${search}`);
  }

  get(id) {
    return http.get(`/movies/${id}`);
  }

  create(data) {
    return http.post("/movies", data);
  }

  update(id, data) {
    return http.put(`/movies/${id}`, data);
  }

  delete(id) {
    return http.delete(`/movies/${id}`);
  }
}

export default new MovieDataService();
